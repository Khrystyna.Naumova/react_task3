function Logo() {
  return (
    <div>
      <img
        src='https://www.graphicsprings.com/filestorage/stencils/67b159abd526cadf2cbd135ce25bb452.png?width=500&height=500'
        alt='logo'
        width='100'
        height='100'
      />
    </div>
  );
}

export default Logo;
