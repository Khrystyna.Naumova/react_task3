import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';
import './Header.css';
import { Link } from 'react-router-dom';

function Header(props) {
  return (
    <div className='header'>
      <Logo />
      {props.token && (
        <div className='headerIn'>
          <h4>Kristen</h4>
          <Link to='/login'>
            <Button buttonText='Logout' handleClick={props.onLogout} />
          </Link>
        </div>
      )}
    </div>
  );
}

export default Header;
