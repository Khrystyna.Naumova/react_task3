import { getCoursesCreator } from './store/courses/actionCreators';
import { getAuthorsCreator } from './store/authors/actionCreators';

export const getCourses = () => {
  return (dispatch) => {
    fetch('http://localhost:4000/courses/all', {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    })
      .then((response) => response.json())
      .then((res) => dispatch(getCoursesCreator(res)));
  };
};

export const getAuthors = () => {
  return (dispatch) => {
    fetch('http://localhost:4000/authors/all', {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    })
      .then((response) => response.json())
      .then((res) => dispatch(getAuthorsCreator(res)));
  };
};
