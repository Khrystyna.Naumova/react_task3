import { LOGIN_SUCCESS, LOGOUT } from './actionTypes';

export const setLoginCreator = (payload) => ({
  type: LOGIN_SUCCESS,
  payload,
});

export const setLogoutCreator = () => ({
  type: LOGOUT,
});
