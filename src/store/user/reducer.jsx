import { LOGIN_SUCCESS, LOGOUT } from './actionTypes';

const userInitialState = {
  isAuth: false, // default value - false. After success login - true
  name: '', // default value - empty string. After success login - name of user
  email: '', // default value - empty string. After success login - email of user
  token: '', // default value - empty string or token value from localStorage.
  // After success login - value from API /login response. See Swagger.
};

export const userReducer = (state = userInitialState, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS:
      let loginState = {
        ...state,
        name: action.payload.user.name,
        email: action.payload.user.email,
        token: action.payload.result,
        isAuth: true,
      };
      return loginState;
    case LOGOUT:
      let logoutState = {
        ...state,
        name: '',
        email: '',
        token: '',
        isAuth: false,
      };
      return logoutState;
    default:
      return state;
  }
};
