import * as actions from './actionTypes';

const authorsInitialState = []; // default value - empty array. After success getting authors from API - array of authors.

export const authorsReducer = (state = authorsInitialState, action) => {
  switch (action.type) {
    case actions.SAVE_AUTHOR:
      return [...state, action.payload];
    case actions.GET_AUTHORS:
      return [...state, ...action.payload.result];
    default:
      return state;
  }
};
