import { configureStore, combineReducers } from '@reduxjs/toolkit';
import { userReducer } from './user/reducer';
import { authorsReducer } from './authors/reducer';
import { coursesReducer } from './courses/reducer';

const rootReducer = combineReducers({
  authors: authorsReducer,
  courses: coursesReducer,
  user: userReducer,
});

export const store = configureStore({
  reducer: rootReducer,
});
