import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
// import { filteredCoursesData } from './helpers/authorConstructor';
import React, { useState } from 'react';
import CreateCourse from './components/CreateCourse/CreateCourse';
import Login from './components/Login/Login';
import Registration from './components/Registartion/Registration';
import { Routes, Route, Navigate, useNavigate } from 'react-router-dom';
import CourseInfo from './components/CourseInfo/CourseInfo';
import { ProtectedRoute } from './components/Login/protectedRoute';
import { v4 as uuidv4 } from 'uuid';

import { setLogoutCreator } from './store/user/actionCreators';
import { useDispatch, useSelector } from 'react-redux';
import { saveNewCourseCreator } from './store/courses/actionCreators';

function App() {
  const [token, setToken] = useState(null);

  const navigate = useNavigate();
  const dispatch = useDispatch();
  console.log(useSelector((state) => state)); /////////////// console

  const coursesData = useSelector((state) => state.courses); // курсы со стора
  const authorsData = useSelector((state) => state.authors);

  const filteredCoursesData = coursesData.map((item) => {
    const authors = item.authors.map((authorId) => {
      const issetId = authorsData.filter((_author) => authorId === _author.id);
      if (issetId.length) {
        return issetId[0].name;
      }
      return authorId;
    });
    return {
      ...item,
      authors,
    };
  });

  const loginHandler = (userToken) => {
    if (userToken === 'Invalid data.' || userToken === undefined) {
      alert('You are not registered yet');
    } else {
      localStorage.setItem('token', JSON.stringify(userToken));
      setToken(userToken);
      navigate('/courses', { replace: true });
    }
  };

  const logoutHandler = () => {
    fetch('http://localhost:4000/logout', {
      method: 'DELETE',
      headers: {
        Authorization: token,
      },
    }).then((res) => console.log(res)); //??

    localStorage.removeItem('token');
    setToken(null);
    dispatch(setLogoutCreator()); ///////////
  };

  const onSaveCourseData = (inputCourseData) => {
    const newCourse = {
      ...inputCourseData,
      id: uuidv4(),
    };
    dispatch(saveNewCourseCreator(newCourse)); /////////////
  };

  return (
    <div>
      <Header token={localStorage.getItem('token')} onLogout={logoutHandler} />
      <Routes>
        <Route
          path='/'
          element={
            localStorage.getItem('token') ? (
              <Navigate to='/courses' />
            ) : (
              <Navigate to='/login' />
            )
          }
        />
        <Route path='/login' element={<Login onLogin={loginHandler} />} />
        <Route path='/registration' element={<Registration />} />
        <Route
          path='/courses'
          element={
            <ProtectedRoute>
              <Courses courses={filteredCoursesData} />
            </ProtectedRoute>
          }
        />
        <Route
          path='/courses/add'
          element={
            <ProtectedRoute>
              <CreateCourse onSaveCourse={onSaveCourseData} />
            </ProtectedRoute>
          }
        />
        <Route
          path='/courses/:courseId'
          element={<CourseInfo courses={filteredCoursesData} />}
        />
        <Route
          path='*'
          element={<p style={{ fontSize: '1.3rem' }}>Page is not found.</p>}
        />
      </Routes>
    </div>
  );
}

export default App;
